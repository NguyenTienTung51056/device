#include <WiFi.h>
#include <WebSocketsClient.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <ESP32_Servo.h>
Servo myservo;

#define DHTPIN 25
#define DHTTYPE DHT11
#define BUZZER_PIN 14
#define FANS 19
#define MOTION_PIN 5

// Define pin numbers
#define LED_N1 22
#define LED_N2 3
#define LED_N3 23
#define LED_C1 26
#define LED_C2 21
#define LED_C3 32
#define LED_C4 33
#define LED1 2
#define LED2 15
#define FLAME_SENSOR 27
#define IR_SENSOR 13
#define GAS_DI 34
#define GAS_AG 35

DHT dht(DHTPIN, DHTTYPE);
WebSocketsClient webSocket;
enum MessageType {
  A_U_T_O,
  POS_180,
  POS_0,
  LED_C1ON,
  LED_C2ON,
  LED_C3ON,
  LED_C4ON,
  LED1_ON,
  LED2_ON,
  LED_C1OFF,
  LED_C2OFF,
  LED_C3OFF,
  LED_C4OFF,
  LED1_OFF,
  LED2_OFF,
  FANS_ON,
  FANS_OFF,
  A_FANS,
  AUTO_MOTION,
  NOTHING
};

// Variables
bool motorShouldTurnOn = false;
bool autoMode = true;
bool led1State = false;
bool led2State = false;
bool ledC1State = false;
bool fansActive = false;
bool autoFans = false;
bool motionAuto = false;
float customTemp = 32;
int pinStateCurrent = LOW;  // current state of pin
int pinStatePrevious = LOW;
int pos = 0;
int servoPin = 18;
const char* ssid = "81 Bui Xuong Trach T1";
const char* password = "68686868";
const char* serverAddress = "periwinkle-witty-shirt.glitch.me";
const int serverPort = 80;
unsigned long previousTime = 0;
const unsigned long interval2 = 399;

void setupWiFi() {
  delay(10);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  WiFi.setSleep(false);

  int attempts = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    attempts++;
    if (attempts >= 15) {
      Serial.println("WiFi connection failed. Rebooting...");
      ESP.restart();  
    }
  }

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void setup() {
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();
  setupWiFi();
  webSocket.begin(serverAddress, serverPort, "/");
  webSocket.onEvent(webSocketEvent);
  myservo.attach(servoPin);
  pinMode(MOTION_PIN, INPUT);
  pinMode(LED_N1, OUTPUT);
  pinMode(LED_N2, OUTPUT);
  pinMode(LED_N3, OUTPUT);
  pinMode(LED_C1, OUTPUT);
  pinMode(LED_C2, OUTPUT);
  pinMode(LED_C3, OUTPUT);
  pinMode(LED_C4, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(FLAME_SENSOR, INPUT);
  pinMode(IR_SENSOR, INPUT);
  pinMode(GAS_DI, INPUT);
  pinMode(GAS_AG, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(FANS, OUTPUT);
  dht.begin();
}

void webSocketEvent(WStype_t type, uint8_t* payload, size_t length) {
  switch (type) {
    case WStype_DISCONNECTED:
      Serial.println("WebSocket disconnected. Reconnecting...");
      break;
    case WStype_CONNECTED:
      Serial.println("Connected to WebSocket server");
      break;
    case WStype_TEXT:
      handleWebSocketMessage(payload, length);
      break;
  }
}



MessageType getMessageType(const char* message) {
  String msg(message);
  if (msg.startsWith("A_U_T_O")) return A_U_T_O;
  if (msg.startsWith("A_FANS")) return A_FANS;
  if (msg.startsWith("POS_180")) return POS_180;
  if (msg.startsWith("POS_0")) return POS_0;
  if (msg.startsWith("LED_C1ON")) return LED_C1ON;
  if (msg.startsWith("LED_C2ON")) return LED_C2ON;
  if (msg.startsWith("LED_C3ON")) return LED_C3ON;
  if (msg.startsWith("LED_C4ON")) return LED_C4ON;
  if (msg.startsWith("LED1_ON")) return LED1_ON;
  if (msg.startsWith("LED2_ON")) return LED2_ON;
  if (msg.startsWith("FANS_ON")) return FANS_ON;
  if (msg.startsWith("LED_C1OFF")) return LED_C1OFF;
  if (msg.startsWith("LED_C2OFF")) return LED_C2OFF;
  if (msg.startsWith("LED_C3OFF")) return LED_C3OFF;
  if (msg.startsWith("LED_C4OFF")) return LED_C4OFF;
  if (msg.startsWith("LED1_OFF")) return LED1_OFF;
  if (msg.startsWith("LED2_OFF")) return LED2_OFF;
  if (msg.startsWith("FANS_OFF")) return FANS_OFF;
  if (msg.startsWith("AUTO_MOTION")) return AUTO_MOTION;
  if (msg.startsWith("NOTHING")) return NOTHING;
  // Default to AUTO if no specific type found
  return NOTHING;
}



void handleWebSocketMessage(uint8_t* payload, size_t length) {
  String message((const char*)payload, length);
  DynamicJsonDocument jsonDoc(1024);
  DeserializationError error = deserializeJson(jsonDoc, message);

  if (error) {
    Serial.print("Failed to parse JSON: ");
    Serial.println(error.c_str());
    return;
  }

  const char* messageValue = jsonDoc["message"];
  const char* clientId = jsonDoc["clientId"];

  Serial.print("Message: ");
  Serial.println(messageValue);
  Serial.print("Client ID: ");
  Serial.println(clientId);

  // Kiểm tra có phải tin nhắn cài nhiệt độ
  if (strncmp(messageValue, "SET_TEMP:", 9) == 0) {

    // Lấy giá trị nhiệt độ từ messageValue
    customTemp = atof(messageValue + 9);
  }

  MessageType messageType = getMessageType(messageValue);

  switch (messageType) {
    case A_U_T_O:
      autoMode = !autoMode;
      if (autoMode) {
        sendJsonMessage("1", "arduino client an", "Auto");
      } else {
        sendJsonMessage("0", "arduino client an", "Auto");
      }
      break;
    case POS_180:
      rotateServo(0, 180, 1);
      if (pos == 180) {
        sendJsonMessage("1", "arduino client an", "servoDevice");
      } else {
        sendJsonMessage("0", "arduino client an", "servoDevice");
      }
      break;
    case POS_0:
      rotateServo(180, 0, -1);
      if (pos == 0) {
        sendJsonMessage("0", "arduino client an", "servoDevice");
      } else {
        sendJsonMessage("1", "arduino client an", "servoDevice");
      }
      break;
    case LED_C1ON:
      digitalWrite(LED_C1, HIGH);
      ledC1State = true;
      if (digitalRead(LED_C1) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LEDC1");
      } else {
        sendJsonMessage("0", "arduino client an", "LEDC1");
      }
      break;
    case LED_C2ON:
      digitalWrite(LED_C2, HIGH);
      if (digitalRead(LED_C2) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LEDC2");
      } else {
        sendJsonMessage("0", "arduino client an", "LEDC2");
      }
      break;
    case LED_C3ON:
      digitalWrite(LED_C3, HIGH);
      if (digitalRead(LED_C3) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LEDC3");
      } else {
        sendJsonMessage("0", "arduino client an", "LEDC3");
      }
      break;
    case LED_C4ON:
      digitalWrite(LED_C4, HIGH);
      if (digitalRead(LED_C4) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LEDC4");
      } else {
        sendJsonMessage("0", "arduino client an", "LEDC4");
      }
      break;
    case LED1_ON:
      led1State = true;
      digitalWrite(LED1, HIGH);
      if (digitalRead(LED1) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LED1");
      } else {
        sendJsonMessage("0", "arduino client an", "LED1");
      }
      break;
    case LED2_ON:
      led2State = true;
      digitalWrite(LED2, HIGH);
      if (digitalRead(LED2) == HIGH) {
        sendJsonMessage("1", "arduino client an", "LED2");
      } else {
        sendJsonMessage("0", "arduino client an", "LED2");
      }
      break;
    case LED_C1OFF:
      digitalWrite(LED_C1, LOW);
      ledC1State = false;
      if (digitalRead(LED_C1) == LOW) {
        sendJsonMessage("0", "arduino client an", "LEDC1");
      } else {
        sendJsonMessage("1", "arduino client an", "LEDC1");
      }
      break;
    case LED_C2OFF:
      digitalWrite(LED_C2, LOW);
      if (digitalRead(LED_C2) == LOW) {
        sendJsonMessage("0", "arduino client an", "LEDC2");
      } else {
        sendJsonMessage("1", "arduino client an", "LEDC2");
      }
      break;
    case LED_C3OFF:
      digitalWrite(LED_C3, LOW);
      if (digitalRead(LED_C3) == LOW) {
        sendJsonMessage("0", "arduino client an", "LEDC3");
      } else {
        sendJsonMessage("1", "arduino client an", "LEDC3");
      }
      break;
    case LED_C4OFF:
      digitalWrite(LED_C4, LOW);
      if (digitalRead(LED_C4) == LOW) {
        sendJsonMessage("0", "arduino client an", "LEDC4");
      } else {
        sendJsonMessage("1", "arduino client an", "LEDC4");
      }
      break;
    case LED1_OFF:
      led1State = false;
      digitalWrite(LED1, LOW);
      if (digitalRead(LED1) == LOW) {
        sendJsonMessage("0", "arduino client an", "LED1");
      } else {
        sendJsonMessage("1", "arduino client an", "LED1");
      }
      break;
    case LED2_OFF:
      led2State = false;
      digitalWrite(LED2, LOW);
      if (digitalRead(LED2) == LOW) {
        sendJsonMessage("0", "arduino client an", "LED2");
      } else {
        sendJsonMessage("1", "arduino client an", "LED2");
      }
      break;
    case FANS_ON:
      digitalWrite(FANS, HIGH);
      fansActive = true;
      ledC1State = true;
      if (digitalRead(FANS) == HIGH) {
        sendJsonMessage("1", "arduino client an", "fans");
      } else {
        sendJsonMessage("0", "arduino client an", "fans");
      }
      break;
    case FANS_OFF:
      digitalWrite(FANS, LOW);
      fansActive = false;
      autoFans = false;
      ledC1State = false;
      if (digitalRead(FANS) == LOW) {
        sendJsonMessage("0", "arduino client an", "fans");
      } else {
        sendJsonMessage("1", "arduino client an", "fans");
      }
      break;
    case A_FANS:
      autoFans = !autoFans;
      if (autoFans) {
        sendJsonMessage("1", "arduino client an", "fans");
      } else {
        sendJsonMessage("0", "arduino client an", "fans");
      }
      break;
    case AUTO_MOTION:
      motionAuto = !motionAuto;
      if (motionAuto) {
        sendJsonMessage("1", "arduino client an", "motionSensor");
      } else {
        sendJsonMessage("0", "arduino client an", "motionSensor");
      }
      break;
    case NOTHING:
      break;
  }
}


void sendJsonMessage(const char* messageText, const char* clientIdText, const char* nameDevice) {
  DynamicJsonDocument jsonDoc(200);

  jsonDoc["message"] = messageText;
  jsonDoc["nameDevice"] = nameDevice;
  jsonDoc["clientId"] = clientIdText;
  jsonDoc["flameValue"] = digitalRead(FLAME_SENSOR);
  jsonDoc["irValue"] = digitalRead(IR_SENSOR);
  jsonDoc["gasDiValue"] = digitalRead(GAS_DI);
  jsonDoc["gasAgValue"] = analogRead(GAS_AG);
  jsonDoc["humidity"] = dht.readHumidity();
  jsonDoc["temperature"] = dht.readTemperature();
  jsonDoc["temperatureF"] = dht.readTemperature(true);

  float hif = dht.computeHeatIndex(jsonDoc["temperatureF"], jsonDoc["humidity"]);
  float hic = dht.computeHeatIndex(jsonDoc["temperature"], jsonDoc["humidity"], false);
  jsonDoc["heatIndexC"] = hic;
  jsonDoc["heatIndexF"] = hif;

  String jsonString;
  serializeJson(jsonDoc, jsonString);

  webSocket.sendTXT(jsonString);
}

void rotateServo(int startPos, int endPos, int step) {
  for (pos = startPos; pos != endPos; pos += step) {
    myservo.write(pos);
    delay(4);
  }
}

void controlLEDs(unsigned long currentTime, int pinStatePrevious, int pinStateCurrent, int irValue, int gasAgValue, int flameValue, float tempicture, bool autoFans, bool autoMode, bool fansActive, float customTemp) {
  digitalWrite(LED1, led1State ? HIGH : LOW);
  digitalWrite(LED2, led2State ? HIGH : LOW);
  digitalWrite(LED_C1, ledC1State ? HIGH : LOW);
  digitalWrite(FANS, fansActive ? HIGH : LOW);
  if (autoFans) {
    float tm = round(tempicture);
    if (tm > customTemp || fansActive) {
      digitalWrite(FANS, HIGH);
      fansActive = true;
      digitalWrite(LED_C1, fansActive ? HIGH : LOW);
    }
  } else {
    digitalWrite(FANS, fansActive ? HIGH : LOW);
    digitalWrite(LED_C1, ledC1State ? HIGH : LOW);
  }

  if (motionAuto) {
    if (pinStatePrevious == LOW && pinStateCurrent == HIGH) {  // pin state change: LOW -> HIGH
      sendJsonMessage("1", "arduino client an", "motionSensor");
      Serial.println("Motion detected!");
      digitalWrite(LED1, HIGH);
      digitalWrite(LED2, HIGH);
      led1State = true;
      led2State = true;
    } else if (pinStatePrevious == HIGH && pinStateCurrent == LOW) {  // pin state change: HIGH -> LOW
      sendJsonMessage("0", "arduino client an", "motionSensor");
      Serial.println("Motion stopped!");
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      led1State = false;
      led2State = false;
    } else {
      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      led1State = false;
      led2State = false;
      digitalWrite(LED1, led1State ? HIGH : LOW);
      digitalWrite(LED2, led2State ? HIGH : LOW);
    }
  } else {
    digitalWrite(LED1, led1State ? HIGH : LOW);
    digitalWrite(LED2, led2State ? HIGH : LOW);
  }

  if (autoMode) {
    if (irValue == HIGH) {
      digitalWrite(LED_N2, HIGH);
    } else {
      digitalWrite(LED_N2, LOW);
    }

    if (gasAgValue >= 2900 && gasAgValue < 3500) {
      digitalWrite(LED_N1, HIGH);
      digitalWrite(LED_N3, LOW);
      digitalWrite(BUZZER_PIN, LOW);
      digitalWrite(LED2, LOW);
    } else if (gasAgValue >= 3500) {
      digitalWrite(LED_N1, LOW);
      digitalWrite(LED_N3, HIGH);
      digitalWrite(BUZZER_PIN, HIGH);
      for (int i = 0; i < 5; i++) {
        digitalWrite(LED1, HIGH);
        delay(200);
        digitalWrite(LED1, LOW);
        delay(200);
        digitalWrite(LED2, HIGH);
        delay(200);
        digitalWrite(LED2, LOW);
      }
    } else {
      digitalWrite(LED_N1, LOW);
      digitalWrite(LED_N3, LOW);
      digitalWrite(BUZZER_PIN, LOW);
      digitalWrite(LED1, led1State ? HIGH : LOW);
      digitalWrite(LED2, led2State ? HIGH : LOW);
    }
  } else {
    digitalWrite(LED_N1, LOW);
    digitalWrite(LED_N2, LOW);
    digitalWrite(LED_N3, LOW);
    digitalWrite(BUZZER_PIN, LOW);
  }

  if (flameValue != HIGH && autoMode) {
    digitalWrite(BUZZER_PIN, HIGH);
    digitalWrite(LED_N3, HIGH);
    for (int i = 0; i < 5; i++) {
      digitalWrite(LED1, HIGH);
      delay(200);
      digitalWrite(LED1, LOW);
      delay(200);
      digitalWrite(LED2, HIGH);
      delay(200);
      digitalWrite(LED2, LOW);
    }
  } else {
    digitalWrite(BUZZER_PIN, LOW);
    digitalWrite(LED_N3, LOW);
    digitalWrite(LED1, led1State ? HIGH : LOW);
    digitalWrite(LED2, led2State ? HIGH : LOW);
  }
}


void loop() {
  webSocket.loop();
  unsigned long currentTime = millis();
  pinStatePrevious = pinStateCurrent;
  pinStateCurrent = digitalRead(MOTION_PIN);
  controlLEDs(currentTime, pinStatePrevious, pinStateCurrent, digitalRead(IR_SENSOR), analogRead(GAS_AG), digitalRead(FLAME_SENSOR), dht.readTemperature(), autoFans, autoMode, fansActive, customTemp);

  if (currentTime - previousTime >= interval2) {
    sendJsonMessage("valueOfDevices", "arduinoclient", "All");
    previousTime = currentTime;
  }
}
